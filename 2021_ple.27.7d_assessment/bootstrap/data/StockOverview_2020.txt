Country	Season	Season type	Year	Stock	Area	Fleets	Effort	UnitEffort	Catch. kg	Catch Cat.	Report cat.	Discards Imported Or Raised
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	533.00	Landings	A-All - reported, nonre	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0	2554518	kWd	9270.40	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	700078	kWd	112605.49	Discards	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	764547	kWd	185435.92	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	1302115	kWd	68668.63	Discards	R-Reported	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
Belgium	4	Quarter	2020	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	2951	kWd	52.00	Landings	R-Reported	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	2841.00	Discards	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	5990.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	13405.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	19284.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	3349.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	5046.00	Landings	A-All - reported, nonre	Imported
Belgium	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	40800	kWd	1831.00	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	16634	kWd	8044.08	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	57238	kWd	19332.22	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	182738	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	58979	kWd	1928.47	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	150747	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	8348	kWd	1353.36	Landings	R-Reported	Imported
UK(Scotland)	2020	Year	2020	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	150700	kWd	7490.00	Landings	R-Reported	Imported
Netherlands	1	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	39670	kWd	2050.00	Landings	R-Reported	Imported
Netherlands	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	10356	kWd	140.00	Landings	R-Reported	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	518.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	7024.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	SDN_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	14540.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	18345.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	9.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	SDN_all_0_0_all	-9	NA	1396.00	Landings	A-All - reported, nonre	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	19771	kWd	4245.33	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	29730	kWd	3255.06	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	42988	kWd	2110.16	Discards	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	DRB_all_0_0_all	6654	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	425929	kWd	13916.87	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	90792	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	2859	kWd	75.40	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	2017	kWd	492.21	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	93101	kWd	6076.22	Discards	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	85759	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	1105.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	70.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	112.00	Landings	A-All - reported, nonre	Imported
Belgium	1	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	8322	kWd	42.00	Landings	R-Reported	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	1906.00	Landings	A-All - reported, nonre	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	1043565	kWd	138874.75	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0	3520290	kWd	14352.68	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0	1525592	kWd	14486.97	Landings	R-Reported	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	241.00	BMS landing	A-All - reported, nonre	Imported
Belgium	4	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0_all	22128	kWd	437.00	Landings	R-Reported	Imported
Belgium	4	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_32-69_0_0_all	10800	kWd	369.00	Landings	R-Reported	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	15009.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	22759.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	11502.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	8024.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	27750.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	6490.00	Landings	A-All - reported, nonre	Imported
Belgium	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0_all	11354	kWd	250.00	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	11681	kWd	3969.55	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	23219	kWd	3558.36	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	40399	kWd	14368.17	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	90826	kWd	4599.29	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	15725	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	82569	kWd	8016.38	Landings	R-Reported	Imported
Netherlands	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	58622	kWd	1030.00	Landings	R-Reported	Imported
Netherlands	4	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF	3922	kWd	110.00	Landings	R-Reported	Imported
Netherlands	4	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	415911	kWd	25110.00	Landings	R-Reported	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	51044.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	8.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	42419.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	124.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	31838.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	58.00	Landings	A-All - reported, nonre	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	101821	kWd	35.98	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	1791145	kWd	53104.37	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	1795	kWd	139.00	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	54203	kWd	7633.33	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	90792	kWd	11504.83	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	27486	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	DRB_MOL_0_0_0_all	2309802	kWd	20085.60	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	1435	kWd	907.30	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	63386	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
Belgium	2020	Year	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	1560369	kWd	742965.00	Landings	R-Reported	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	719.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	14.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	70.00	Landings	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	1.00	BMS landing	A-All - reported, nonre	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	1302115	kWd	125330.68	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	700078	kWd	234237.03	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0	1765423	kWd	11523.14	Landings	R-Reported	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	277.00	Landings	A-All - reported, nonre	Imported
Belgium	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_32-69_0_0_all	10800	kWd	28.00	Landings	R-Reported	Imported
Belgium	1	Quarter	2020	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	1537	kWd	1473.00	Landings	R-Reported	Imported
Netherlands	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	11754	kWd	230.00	Landings	R-Reported	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	101.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	2671.00	Discards	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	18497.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	1395	kWd	15.00	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	150747	kWd	8861.12	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_32-69_0_0	23219	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	182738	kWd	7876.38	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	1019	kWd	20.00	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	57130	kWd	6745.14	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	40399	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
Netherlands	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_100-119_0_0_all	1313	kWd	10.00	Landings	R-Reported	Imported
Netherlands	3	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	109178	kWd	3870.00	Landings	R-Reported	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	SDN_all_0_0_all	-9	NA	639.00	Landings	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	93101	kWd	11337.78	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	42988	kWd	4779.73	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	90792	kWd	3994.75	Discards	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	28702	kWd	681.19	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	4555	kWd	35.40	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	5593	kWd	251.00	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	DRB_all_0_0_all	6654	kWd	3262.03	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	12929	kWd	734.46	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	85759	kWd	12790.18	Landings	R-Reported	Imported
Belgium	2020	Year	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	1560369	kWd	1225024.00	Discards	R-Reported	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0	2554518	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	700078	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0	1302115	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	-9	NA	5407.00	Landings	A-All - reported, nonre	Imported
Belgium	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	6000	kWd	798.00	Landings	R-Reported	Imported
Belgium	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_32-69_0_0_all	23859	kWd	242.00	Landings	R-Reported	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_70-99_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	5.00	Landings	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
Belgium	4	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	27600	kWd	1540.00	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	164439	kWd	7922.42	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	58979	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	57238	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	15725	kWd	4669.81	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_120-219_0_0_all	3918	kWd	113.00	Landings	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	718	kWd	21.91	Landings	R-Reported	Imported
Germany	4	Quarter	2020	ple.27.7d	27.7.d	OTB_CRU_70-99_0_0_all	12099	kWd	555.00	Landings	R-Reported	Imported
Netherlands	4	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	649269	kWd	450.00	Landings	R-Reported	Imported
Netherlands	1	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_70-99_0_0_all	382071	kWd	18460.00	Landings	R-Reported	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	GNS_DEF_all_0_0_all	-9	NA	10.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	TBB_DEF_70-99_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	SSC_DEF_All_0_0_All	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	34.00	Landings	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	4	Quarter	2020	ple.27.7d	27.7.d	LLS_FIF_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	1	Quarter	2020	ple.27.7d	27.7.d	SDN_all_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	DRB_all_0_0_all	1476	kWd	460.21	Landings	R-Reported	Imported
France	2	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	27486	kWd	10100.38	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_100-119_0_0_all	42988	kWd	0.00	Logbook Registered Discard	R-Reported	Imported
France	3	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	18041	kWd	877.76	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_90-99_0_0_all	63386	kWd	5108.98	Landings	R-Reported	Imported
France	1	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_<16_0_0_all	1577	kWd	200.00	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_120-219_0_0_all	33639	kWd	865.02	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	GTR_DEF_all_0_0_all	16845	kWd	340.88	Landings	R-Reported	Imported
France	4	Quarter	2020	ple.27.7d	27.7.d	OTT_DEF_70-99_0_0	9684	kWd	4711.58	Landings	R-Reported	Imported
UK(Scotland)	2020	Year	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	32689	kWd	6.00	Landings	R-Reported	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	FPO_CRU_0_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	2	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
UK (England)	3	Quarter	2020	ple.27.7d	27.7.d	OTB_DEF_>=120_0_0_all	-9	NA	0.00	BMS landing	A-All - reported, nonre	Imported
Belgium	4	Quarter	2020	ple.27.7d	27.7.d	MIS_MIS_0_0_0_HC	5694	kWd	65.00	Landings	R-Reported	Imported
