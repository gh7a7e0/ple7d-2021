# --------------------------------------------------------------------------------------
# Functions for the diagnostics of the North Sea plaice assessment
#
# Author  : Jan Jaap Poos
#           (+David Miller)
#
# Last edited: MAY 2011
# --------------------------------------------------------------------------------------
########---------------------------+INDEX+------------------------------########
## STF
#  scanSTF: Runs STFs for a range of Fmult values
#  summTableSTF: Creates a summary from 'scanSTF' outputs
#  inputTableSTF: Creates a table of inputs to the STF
#  ageTableSTF: Creates a table by year AND AGE for the projection period 


########--------------------------+++++++++-----------------------------########
####                                 STF                                    ####
########--------------------------+++++++++-----------------------------########
# NAME: scanSTF        
# DOES: Runs STFs for a range of Fmult or F (mean) values  (if fmult=T, fscan used us fmult, else fscan used as absolute levels of F0
# Recoded for R 2.12.2 (D.Miller)
#stfObj <- stf_Object; projControl<- stf_Control; fscan=c(0.8, 1.0, 1.2); SRR=srr
scanSTF <- function(stfObj, projControl="missing", SRR=NA, fscan=c(0.8, 1.0, 1.2), fmult=T){
    if (missing(projControl)) stop("no projControl")
    if (!inherits(stfObj, "FLStock") ) stop("stfObj must be 'FLStock' or 'FLSTF' object")
    
    FSQ <- as.numeric(fbar(stfObj)[,ac(range(stfObj)["maxyear"])])
    
    res <- list()
    rIt <- 0
    for (it in fscan) {
       rIt <- rIt+1
       cat("Fmult: ", it, "\n")
       if (fmult) itVal <- it*FSQ else itVal <- it
       if (length(projControl@target) == 1) {
         projControl@target[1,"val"] <- itVal  
         } else {
         projControl@target[2:length(projControl@target),"val"] <-  itVal      
         }
       tmp <- project(stfObj, projControl, srr)
       res[[paste(rIt,it,sep="_")]] <- window(tmp, start=stfObj@range["maxyear"]-2, end=stfObj@range["maxyear"])
    }
    return(res)
}
########--------------------------+++++++++-----------------------------########
# NAME: summTableSTF
# DOES: Creates a summary from 'scanSTF' outputs
# object <- fwd_Fsq_Options
summTableSTF  <- function(object){
    d.f <- NULL
    for (i in 1:length(object) ) {
        fmult <- as.numeric(strsplit(names(object)[i],"_")[[1]][2])
        fbar.min <- as.numeric(range(object[[i]])["minfbar"])
        fbar.max <- as.numeric(range(object[[i]])["maxfbar"])
        ssb. <- as.data.frame(round(ssb(object[[i]]),0))
        f.   <- as.matrix(c(round(apply(slot(object[[i]], "harvest")[as.character(fbar.min:fbar.max),,,,], 2:5, mean),3)))
        r.   <- as.matrix(c(round(object[[i]]@stock.n[1,],0)))
        c.   <- as.matrix(c(round(object[[i]]@catch,0)))    
        f.d  <- as.matrix(c(round(apply((slot(object[[i]], "harvest") * object[[i]]@discards.n/object[[i]]@catch.n)[as.character(2:3),], 2:5, mean),2)))
        f.l  <- as.matrix(c(round(apply((slot(object[[i]], "harvest") * object[[i]]@landings.n/object[[i]]@catch.n)[as.character(fbar.min:fbar.max),,,,], 2:5, mean),2)))
        l.   <- as.matrix(c(round(apply(object[[i]]@landings.n*object[[i]]@landings.wt,2,sum, na.rm=TRUE),0)))
        d.   <- as.matrix(c(round(apply(object[[i]]@discards.n*object[[i]]@discards.wt,2,sum, na.rm=TRUE),0)))
        d.f  <- rbind(d.f, cbind(fmult, ssb., f., f.d, f.l, r., c., l., d.))
    }
    colnames(d.f) <- c("fmult", "age","year","unit","season","area","iter","ssb",
    paste("f",fbar.min,"-",fbar.max,sep=""),"f_dis2-3", "f_hc2-6","recruit", "catch", "landings", "discards")
    return(d.f)
}

########--------------------------+++++++++-----------------------------########
# NAME: inputTableSTF
# DOES: Creates a table of inputs to the STF
# E.G.# object <- window(ple4.stf,start=finalyear+1, end=finalyear+3)
inputTableSTF <- function(object){
  #note: for reasons of convenience I have removed calculation and output of fdisc and fland
  f.  <- as.data.frame(round(object@harvest,3))
  fd.   <- as.data.frame(round(object@harvest*(object@discards.n/object@catch.n),2))[,"data"]
  fl.   <- as.data.frame(round(object@harvest*(object@landings.n/object@catch.n),2))[,"data"]
  n.  <- as.data.frame(round(object@stock.n,0))[,"data"]
  cw. <- as.data.frame(round(object@catch.wt,2))[,"data"]
  lw. <- as.data.frame(round(object@landings.wt,2))[,"data"]
  dw. <- as.data.frame(round(object@discards.wt,2))[,"data"]
  sw. <- as.data.frame(round(object@stock.wt,2))[,"data"]
  mat.<- as.data.frame(object@mat)[,"data"]
  m.  <- as.data.frame(object@m)[,"data"]
  d.f <- cbind(f., fd., fl., n., cw., lw., dw.,sw., mat., m.)[,c(-3,-4,-5)]
  colnames(d.f) <- c("age",	"year",	"NA","f","f.disc","f.land",  "stock.n"	, "catch.wt", "landings.wt", "discards.wt","stock.wt", "mat", "M")
  return(d.f)
}
########--------------------------+++++++++-----------------------------########
# NAME: ageTableSTF
# DOES: Creates a table by year AND AGE for the projection period
ageTableSTF <- function(object){
  catch.n    <- as.data.frame(round(object@catch.n))[,"data"]
  catch      <- as.data.frame(round(object@catch.n*object@catch.wt,0))[,"data"]
  landings.n <- as.data.frame(round(object@landings.n,0))[,"data"]
  landings   <- as.data.frame(round(object@landings.n*object@landings.wt,0))[,"data"]
  discards.n <- as.data.frame(round(object@discards.n,0))[,"data"]
  discards   <- as.data.frame(round(object@discards.n*object@discards.wt,0))[,"data"]
  SSB        <- as.data.frame(round(object@stock.n*object@stock.wt*object@mat,0))[,"data"]
  TSB        <- as.data.frame(round(object@stock.n*object@stock.wt,0))[,"data"]
  inputtable <- inputTableSTF(object)
  d.f <- cbind(inputtable, catch.n, catch, landings.n, landings, discards.n, discards, SSB, TSB)
  return(d.f)
}

########--------------------------+++++++++-----------------------------########


